﻿using UnityEngine;
using System.Collections;

namespace CoAHomework
{
    public class HA_2 : MonoBehaviour
    {
        public bool isGrounded;
        public bool canJump;

        void Update()
        {
            if (isGrounded == false)
            {
                canJump = false;
            }
            else
            {
                canJump = true;
            }
            Debug.Log(canJump);
        }
    }
}
