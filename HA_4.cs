﻿using UnityEngine;
using System.Collections;

namespace CoAHomework
{
    public class HA_4 : MonoBehaviour
    {
        public Playerstate playerstate;
        private float movementSpeed;

        void Update()
        {
            switch(playerstate)
            {
                case Playerstate.NONE:
                    movementSpeed = 0;
                    Debug.Log(movementSpeed);
                    Debug.Log(playerstate);
                    break;

                case Playerstate.IDLE:
                    movementSpeed = 0;
                    Debug.Log(movementSpeed);
                    Debug.Log(playerstate);
                    break;

                case Playerstate.WALKING:
                    movementSpeed = 10;
                    Debug.Log(movementSpeed);
                    Debug.Log(playerstate);
                    break;

                case Playerstate.RUNNING:
                    movementSpeed = 20;
                    Debug.Log(movementSpeed);
                    Debug.Log(playerstate);
                    break;

            }
        }
    }
}
