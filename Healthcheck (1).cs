﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Healthcheck : MonoBehaviour
    {

        public bool isPlayerAlive;
        public int health = 100;

        void Update()
        {
            if (isPlayerAlive)
            {
                Debug.Log(health);

                if (health == 10)
                {
                    Debug.Log(message: "You are badly hurt!");

                }
                else if (health < 10)
                {
                    Debug.Log(message: "Death is whispering to you?");
                }
            }
        }
    }
}
